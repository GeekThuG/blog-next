import Link from "next/link"
import "bootstrap/dist/css/bootstrap.min.css";

export default function index({users}) {
  return (
    <div className="container px-4 pt-5">
        <Link href={'/'}> <button>Retour</button></Link>
        <h1 className="text-center">
            La liste des utilisateurs
        </h1>
        <div className="row justify-content-center mt-5">
            {users.map(user => (
                <div key={user.id} className="col-12 col-lg-6 m-3">
                    <div className="card">
                        <div className="card-body d-flex justify-content-between">
                            <h5 className="card-title">
                                {user.username}
                            </h5>
                            <Link href={`/utilisateurs/${user.id}`}>
                                <span className="ml-auto card-link">Contacter</span>
                            </Link>
                        </div>
                    </div>
                </div>
            ))}
        </div>
      
    </div>
  )
}

export async function getStaticProps() {
    const data = await fetch("https://jsonplaceholder.typicode.com/users");
  const users = await data.json();

  return {
    props: {
        users
    }
  }
}
